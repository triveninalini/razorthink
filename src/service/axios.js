import axios from "axios";
import config from "../config.json";

const instance = axios.create({
  baseURL: config.serverUrl,
  headers: { Authorization: `Client-ID ${config.accessKey}` }
});

export default instance;