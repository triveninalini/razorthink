import axios from "./axios";
import config from "../config.json";

export async function getPhotos(params) {
  try {
    const url = config.api.getPhotos + getUrl(params);
    const response = await axios.get(url);
    return response.data;
  } catch (ex) {
    console.error(`unable to fetch photos`);
    // eslint-disable-next-line no-throw-literal
    throw { success: false };
  }
}

export async function getPhoto(userName) {
  try {
    const response = await axios.get(`${config.api.getPhotos}/${userName}`);
    return response.data;
  } catch (ex) {
    console.error("error in getting user", ex);
    throw { success: false }
  }
}
function getUrl(params) {
  let flag = true, url = "";
  for (let key in params) {
    if (params.hasOwnProperty(key))
      if (flag) url += `?${key}=${params[key]}`;
      else url += `&${key}=${params[key]}`;
    flag = false;
  }
  return url;
}

