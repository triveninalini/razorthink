import React from 'react';
import './App.css';
import Home from "./components/home";
import ImageView from "./components/imageView";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


function App() {
  return <Router>
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/images/:id">
        <ImageView />
      </Route>
    </Switch>
  </Router>
}

export default App;
