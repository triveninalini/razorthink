import React, { useState, useEffect } from "react";
import { getPhotos } from "../service/service";
import { Link } from "react-router-dom";


function Home() {
  const [photos, setPhotos] = useState([]);
  const [page, setPage] = useState(1);
  const [query, setQuery] = useState("");

  async function getImages(isPagination) {
    const images = await getPhotos({ query, page });
    if (isPagination) {
      setPhotos([...photos, ...images])
      return;
    }
    setPhotos(images);
  }

  useEffect(() => {
    getImages();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query]);


  useEffect(() => {
    getImages(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  return (
    <div className="container">
      <div className="search-bar">
        <input className="search" placeholder="Search for Images here" onChange={(e) => setQuery(e.target.value)} />
        <div className="search-icon">
          <i class="fa fa-search"></i>
        </div>
      </div>
      <div className="image-container">
        {photos.map((photos = {}) => {
          const user = photos.user || {};
          return <Link to={`images/${photos.id}`}><div className="image-item">
            <img src={(photos.urls || {}).regular} alt="image" />
            <div className="user-container">
              <img src={user.profile_image.small} />
              <p>Image by <span>{user.first_name} {user.last_name}</span></p>
            </div>
          </div></Link>
        })}
      </div>
      <div className="button-container">
        <button onClick={() => setPage(page + 1)}>Load More</button>
      </div>
    </div>
  );
}

export default Home;