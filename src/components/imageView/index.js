/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import "./style.css";
import { getPhoto } from "../../service/service";

function ImageView(props) {
  const [photo, setPhoto] = useState({});
  const params = useParams();

  useEffect(() => {

    async function fetchPhoto() {
      const userData = await getPhoto(params.id);
      setPhoto(userData);
    }
    fetchPhoto();
  }, [params.id]);

  function downloadPhoto() {
    const element = document.createElement("a");
    const file = new Blob(
      [
        photo.links.download_location
      ],
      { type: "image/*" }
    );
    element.href = URL.createObjectURL(file);
    element.download = "image.jpg";
    element.click();
  }

  const { user = {} } = photo;

  return <div className="image-view-container">
    <div className="content">
      <Link to="/"><i class="fa fa-times-circle fa-3x close-icon" aria-hidden="true"></i></Link>
      <div className="image-view-user-container">
        <img src={(user.profile_image || {}).small} />
        <div className='image-view-user-content'>
          <p>{user.name}</p>
          <span>{user.instagram_username}</span>
        </div>
      </div>
      <div className="view-container">
        <img src={(photo.urls || {}).full} />
      </div>
      <div className="button-container">
        <button onClick={() => downloadPhoto(photo)}>Download</button>
      </div>
    </div>
  </div>
}

export default ImageView;